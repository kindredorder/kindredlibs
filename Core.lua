-- User: Timetick
-- Date: 1/26/2017
-- Time: 9:40 PM
local MAJOR, MINOR = "KindredLibs", 1

assert(_G.LibStub, MAJOR .. " requires LibStub")
local lib = _G.LibStub:NewLibrary(MAJOR, MINOR)
if not lib then return end

-- constants
local _G = _G

local colorIndex = {10, 20, 30, 40, 50, 60, 70, 80, 90, 100}
local colorScale = {
    -- Gray for <= 10%
    [10] =       {['r']=127,     ['g']=127,      ['b']=127,      ['hex']='7F7F7F',   ['p']=10,},
    -- Dark Red for > 20%
    [20] =       {['r']=63,     ['g']=0,      ['b']=0,      ['hex']='3F0000',   ['p']=10,},
    -- Red for > 30%
    [30] =       {['r']=255,     ['g']=0,      ['b']=0,      ['hex']='FF0000',   ['p']=20,},
    -- Yellow for > 40%
    [40] =     {['r']=255,     ['g']=255,      ['b']=0,        ['hex']='FFFF00',   ['p']=30,},
    -- Green for > 50%
    [50] =     {['r']=0,       ['g']=255,      ['b']=0,        ['hex']='00FF00',   ['p']=40,},
    -- Teal for > 60%
    [60] =     {['r']=0,       ['g']=255,      ['b']=255,      ['hex']='00FFFF',   ['p']=50,},
    -- Blue for > 70%
    [70] =     {['r']=0,       ['g']=102,      ['b']=255,      ['hex']='0066FF',   ['p']=60,},
    -- Purple for > 80%
    [80] =     {['r']=255,     ['g']=0,        ['b']=255,      ['hex']='FF00FF',   ['p']=70,},
    -- Legendary Orange for > 90%
    [90] =    {['r']=255,     ['g']=128,        ['b']=0,        ['hex']='FF8000',   ['p']=80,},
    -- Pink for >= 100%
    [100] =    {['r']=255,     ['g']=128,        ['b']=0,        ['hex']='FF8000',   ['p']=90,},
}

local function isNum(num)
    return type(num) == "number"
end

-- from http://www.wowpedia.org/RGBPercToHex
local function RGBtoHex (r, g, b)
    if isNum(r) and isNum(g) and isNum(b) then
        if r < 0 or r > 1 then
            r = 0
        end
        if g < 0 or g > 1 then
            g = 0
        end
        if b < 0 or b > 1 then
            b = 0
        end
    else
        r = 0
        g = 0
        b = 0
    end
    return string.format("%02x%02x%02x", r*255, g*255, b*255)
end

lib.getColorFromScale = function(value, maxValue)
    local found = false
    local percent = math.min(100, math.floor(value / maxValue * 100))

    local r = 1
    local g = 1
    local b = 1

    for index,step in pairs(colorIndex) do
        if percent <= step and not found then
            local colors = colorScale[step]
            local baseColors = colorScale[colors.p]

            local steps = step - colors.p
            local scoreDiff = percent - colors.p

            local diffR = (baseColors.r - colors.r) / 255
            local diffG = (baseColors.g - colors.g) / 255
            local diffB = (baseColors.b - colors.b) / 255

            local diffStepR = diffR / steps
            local diffStepG = diffG / steps
            local diffStepB = diffB / steps

            local scoreDiffR = scoreDiff * diffStepR
            local scoreDiffG = scoreDiff * diffStepG
            local scoreDiffB = scoreDiff * diffStepB

            r = (baseColors.r / 255) - scoreDiffR
            g = (baseColors.g / 255) - scoreDiffG
            b = (baseColors.b / 255) - scoreDiffB

            found = true
        end
    end
    return RGBtoHex(r, g, b), r, g, b
end

lib.PrintR = function(t, returnOnly)
    local print_r_cache={}
    local ret = ""
    local function sub_print_r(t,indent)
        if (print_r_cache[tostring(t)]) then
            if not returnOnly then
                _G.print(indent.."*"..tostring(t))
            else
                ret = ret .. indent .."*".. tostring(t) .. "\n"
            end
        else
            print_r_cache[tostring(t)]=true
            if (type(t)=="table") then
                for pos,val in pairs(t) do
                    if (type(val)=="table") then
                        if not returnOnly then
                            _G.print(indent.."["..pos.."] => "..tostring(t).." {")
                        else
                            ret = ret .. indent .. "[" .. pos.. "] => " .. tostring(t) .. " {" .. "\n"
                        end
                        sub_print_r(val,indent..string.rep(" ",string.len(pos)+8))
                        if not returnOnly then
                            _G.print(indent..string.rep(" ",string.len(pos)+6).."}")
                        else
                            ret = ret .. indent .. string.rep(" ", string.len(pos) + 6) .. "}" .. "\n"
                        end
                    elseif (type(val)=="string") then
                        if not returnOnly then
                            _G.print(indent.."["..pos..'] => "'..val..'"')
                        else
                            ret = ret .. indent .. "[" .. pos .. '] => "' .. val .. '"' .. "\n"
                        end
                    else
                        if not returnOnly then
                            _G.print(indent.."["..pos.."] => "..tostring(val))
                        else
                            ret = ret .. indent .. "[" .. pos .. "] => " .. tostring(val) .. "\n"
                        end
                    end
                end
            else
                if not returnOnly then
                    _G.print(indent..tostring(t))
                else
                    ret = ret .. indent .. tostring(t) .. "\n"
                end
            end
        end
    end
    if (type(t)=="table") then
        if not returnOnly then
            _G.print(tostring(t).." {")
        else
            ret = ret .. tostring(t) .. " {" .. "\n"
        end
        sub_print_r(t,"  ")
        if not returnOnly then
            _G.print("}")
        else
            ret = ret .. "}" .. "\n"
        end
    else
        sub_print_r(t,"  ")
    end
    if not returnOnly then
        _G.print()
    else
        return ret
    end
end
