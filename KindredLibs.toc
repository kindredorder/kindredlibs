## Interface: 72000
## Title: KindredLibs
## Notes: A collection of utilities used in addons developed for the Kindred Guild on Malygos(US)
## Author: Timetick <Kindred> US/Malygos
## Version: @project-version@

embeds.xml
KindredLibs.xml

